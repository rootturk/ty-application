﻿using CachingFramework.Redis;
using CachingFramework.Redis.Contracts.RedisObjects;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Trendyol.Model.CacheModel;
using Trendyol.Service.Collections;
using Trendyol.Service.Service;
using TrendyolWinservice.CacheModel;

namespace TrendyolWinService
{
    class Program
    {
        static void Main(string[] args)
        {
            
            #region Init Function
            init();
            #endregion

            ProductService service = new ProductService();
            CacheService redisService = new CacheService(new ProductCollection());

            redisService.clearProductCache();

            while (true)
            {
                int lastId  = redisService.setProductsToCache(service.getAllProducts(ConsoleConfiguration.redisLastRecord));

                if(lastId > 0)
                {
                    ConsoleConfiguration.redisLastRecord = lastId;
                }

                if (ConsoleConfiguration.lastDateTime != null)
                {
                   redisService.updateProductsByLastUpdateDate(service.getProductsByDate((DateTime)ConsoleConfiguration.lastDateTime));
                }

                ConsoleConfiguration.lastDateTime = DateTime.Now;

                Thread.Sleep(60*5*1000);


            }
            
        }

        private static void init()
        {
            Console.Write("Trendyol Windows Service v0.0.1...");           
        }

    }
}

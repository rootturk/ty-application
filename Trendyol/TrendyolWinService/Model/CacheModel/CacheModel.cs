﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendyolWinservice.CacheModel
{

    [Serializable]
    public static class ConsoleConfiguration
    {
        public static int redisLastRecord { get; set; }
        public static DateTime? lastDateTime { get; set; }
        public static string key { get { return "config"; } }

    }
}

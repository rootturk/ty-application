﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Trendyol.Model.CacheModel;
using Trendyol.Service.Collections;
using Trendyol.Service.Service;

namespace TrendyolMVC.Controllers
{
    public class ProductController : Controller
    {
        CacheService _cacheService = new CacheService(new ProductCollection());
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int page=1)
        {
            int pageSize = 10;

            IEnumerable<Products> products = _cacheService.getProductsFromCache().Take(pageSize);

            return View(products.ToList());
        }
        
        [HttpPost]
        public void getProducts(int size)
        {
            int pageSize = 10;
            IEnumerable<Products> products = _cacheService.getProductsFromCache().Skip(size).Take(10);
            JavaScriptSerializer js = new JavaScriptSerializer();

            Response.Write(js.Serialize(products));
        }

        public ActionResult ListDemo()
        {
            return View();
        }
    }
}
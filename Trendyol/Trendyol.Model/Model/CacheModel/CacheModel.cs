﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trendyol.Model.CacheModel
{

    [Serializable]
    public class Products
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trendyol.Model.CacheModel;

namespace Trendyol.Service.Collections
{
    public class ProductCollection : BaseCollection<Products>
    {
        public ProductCollection() : base("product_list")
        {

        }
    }
}

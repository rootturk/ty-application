﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CachingFramework.Redis;
using CachingFramework.Redis.Contracts.RedisObjects;
namespace Trendyol.Service.Collections
{
   public abstract class BaseCollection<T>
    {
        protected IRedisList<T> list;
        protected Dictionary<int, int> mapper = new Dictionary<int, int>();
        public BaseCollection(string listName)
        {
            var context = new Context();
            list = context.Collections.GetRedisList<T>(listName);
        }
        public void Add(int id, T item)
        {
            list.Add(item);
            mapper.Add(id, mapper.Count);
        }
        public IEnumerable<T> getAll(int startIndex, int lenght)
        {
            return list.GetRange(startIndex, lenght);
        }
        public void update(int id, T item)
        {
            int index = mapper[id];
            list[index] = item;
        }
        public void clear()
        {
            list.Clear();
        }

    }
}

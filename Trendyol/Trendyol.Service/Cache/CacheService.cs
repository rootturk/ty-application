﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trendyol.Model.CacheModel;
using Trendyol.Model.Data;
using Trendyol.Service.Collections;

namespace Trendyol.Service.Service
{
    public class CacheService
    {
        private ProductCollection productCollection;
        public CacheService(ProductCollection productCollection)
        {
            this.productCollection = productCollection;
        }

        /// <summary>
        ///m
        /// </summary>
        /// <param name="products"></param>
        /// <returns></returns>
        public int setProductsToCache(List<tblProducts> products)
        {

            int val = 0;
            foreach (var item in products)
            {
                Products prod = new Products();
                prod.Id = item.Id;
                prod.LastUpdatedTime = item.LastUpdatedTime;
                prod.ProductName = item.ProductName;

                val = prod.Id;

                productCollection.Add(val, prod);
            }

            return val;
        }

        public void clearProductCache() {
            productCollection.clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Products> getProductsFromCache()
        {
            return productCollection.getAll(0, 30);
        }

        public void updateProductsByLastUpdateDate(List<tblProducts> products)
        {
            foreach (tblProducts product in products)
            {
                Products prod = new Products();
                prod.Id = product.Id;
                prod.LastUpdatedTime = product.LastUpdatedTime;
                prod.ProductName = product.ProductName;

                productCollection.update(prod.Id, prod);

            }
        }
    }
}

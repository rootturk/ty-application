﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Trendyol.Model.Data;

namespace Trendyol.Service.Service
{
    public class ProductService : DB
    {
        public List<tblProducts> getAllProducts(int lastId)
        {
            return _entities.tblProducts.Where(x => x.Id > lastId).ToList();
        }

        public List<tblProducts> getProductsByDate(DateTime lastUpdate)
        {
            return _entities.tblProducts.Where(x => x.LastUpdatedTime > lastUpdate).ToList();
        }
    }
}